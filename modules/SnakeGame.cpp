#include "SnakeGame.h"
#include "winCursor.h"
#include <time.h>   
#include <stdlib.h>  

int SnakeGame::generatePos()
{
    int result = rand() % (m*n);

    while( gameSnake->searchVt(result) == 1)
    {
         result = rand() % (m*n);
    }
    return result;
}

SnakeGame::SnakeGame(int height, int width)
{
    srand(time(NULL));
    m = height;
    n = width;
    gamePlayer = new Player();
    gamePlayer->readHighScore();
    gameSnake = new Snake();
    gameCourse = new SnakeCourse(m, n);
    apple = new Apple(10);
    apple->setPos(generatePos());
    cherry = new Cherry(20, 150);
    cherry->setPos(generatePos());
    lastDirection = rightD;
    invisCursor();
}

void SnakeGame::deleteObjs()
{
    gamePlayer->~Player();
    gamePlayer = NULL;
    gameSnake->~Snake();
    gameSnake = NULL;
    apple->~Apple();
    apple = NULL;
    cherry->~Cherry();
    cherry = NULL;
    gameCourse->~SnakeCourse();
    gameCourse = NULL;
}

void SnakeGame::resetGame()
{

    gameSnake->resetSnake();
    gamePlayer->resetCurrScore();
    apple->resetFruit(10);
    apple->setPos(generatePos());
    cherry->resetCherry(20, 150);
    cherry->setPos(generatePos());
    gameCourse->clearMatrix();
    lastDirection = rightD;
}

Direction SnakeGame::nextDirection()
{
    cin.clear();
    char input = 0;
    volatile int timer = 0;
    while(timer++ < 400)
    {
        if(kbhit()) 
        {
            input = getch();
        
            switch(input)
            {
                    case 75:
                        return leftD;
                    case 72:
                        return upD;
                    case 77:
                        return rightD;
                    case 80:
                        return downD;
                    case 3:
                        exit(0);
            } 
        }
        timer++;
    }
    return lastDirection;

}


char SnakeGame::nextTurn()
{
    char choice = 0;
    Direction dir = nextDirection();

   if((dir == upD && lastDirection == downD) ||  (dir == downD && lastDirection == upD) ||
   (dir == leftD && lastDirection == rightD) ||  (dir == rightD && lastDirection == leftD))
   {
       dir = lastDirection;
   }

    lastDirection = dir;
    int nextStep = gameSnake->getLast();

    switch(dir)
    {
        case leftD:
            nextStep -= 1;
            if((nextStep+1) % n == 0)
            {
                nextStep = nextStep + n;
            }
            break;

        case upD:
            nextStep -= n;
            if(nextStep < 0)
            {
                nextStep = (m*n) + nextStep;
            }
            break;

        case rightD:
            nextStep += 1;
            if(nextStep % n == 0)
            {
                nextStep = nextStep - n;
            }
            break;

        case downD:
            nextStep += n;
            if(nextStep >= m*n)
            {
                nextStep = nextStep%n;
            }
            break;
    }
    
    gameSnake->growSnake(nextStep);

    gotoxy(0, 0);
    cout << "Length: " << gameSnake->getLength() << '\n';
    cout << "CURRENT SCORE: " << gamePlayer->getCurrScore() << "    HIGH SCORE: " << gamePlayer->getHighScore() << '\n';

    switch(gameCourse->drawCourse(gameSnake->getSnake(), apple->getPos(), cherry->getPos()))
    {
        case -1:
            cout << '\n';
            cout << "GAME OVER\n";
            cout << "Press enter for new game, ctrl-c to quit\n";

            cin.sync(); cin.clear();
            
            while(1)
            {   
                gamePlayer->saveHighScore();
                choice = getch();
                if(choice == 3) //Ctrl c 
                {
                    return -1;
                }
                else if(choice == 13) //Enter ♪
                {
                    return 1;
                }
            }
            break;

        case 0:
            gameSnake->popTail();

            if(cherry->isSpawned() == 1)
            {
                cherry->minusLifetime();
            }
            else
            {
                cherry->respawn(1+rand()%200);
                if(cherry->isSpawned() == 1)
                {
                    cherry->setPos(generatePos());
                }
            }
            break;
        case 1:
            gamePlayer->addCurrScore(apple->getPoints());
            apple->setPos(generatePos());
            break;
        case 2:
            gamePlayer->addCurrScore(cherry->getPoints());
            cherry->despawn();
            break;
    }

    return 0;
}

void SnakeGame::startGame()
{
    while(1)
        switch(nextTurn())
        {
            case -1:
                deleteObjs();
                exit(0);
            case 0: 
                break;
            case 1:
                gamePlayer->saveHighScore();
                resetGame();
                system("CLS");
        }
            
}