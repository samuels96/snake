#include "Course.h"

Course::~Course()
{
    for(int i = 0; i < n+2; i++)
    {
        delete[] matrixCourse[i];
}
    delete[] matrixCourse;
}



void SnakeCourse::clearMatrix()
{
    for(int i = 0; i < m+2; i++){
        for(int j = 0; j < n+2; j++)
            {
            if(i==0 || i == m+1) { matrixCourse[i][j] = 196; continue; }
            else if(j == 0 || j == n+1) { matrixCourse[i][j] = 179; continue;}
            else matrixCourse[i][j] = ' ';
            }
    }

    matrixCourse[0][0] = 218;
    matrixCourse[0][n+1] = 191;
    matrixCourse[m+1][0] = 192;
    matrixCourse[m+1][n+1] = 217;
}

SnakeCourse::SnakeCourse(int m, int n)
{
    
    this->m = m;
    this->n = n;

    matrixCourse = new unsigned char*[n+2];


    for(int i = 0; i < n+2; i++)
    {
        matrixCourse[i] = new unsigned char[m+2];
    }

    clearMatrix();
}

int detectRow(int pos, int n)
{
    pos += n;

    for(int i = 1; i <= n+1; i++)
    {
        if(pos < i*n)
        {
            return i-1;
        }
        else if(pos == i*n)
        {
            return i;
        }
    }

    return 1;
}

void SnakeCourse::rewriteAt(int i, unsigned char val)
{
    matrixCourse[detectRow(i, n)][i%n+1] = val;
}

char SnakeCourse::drawCourse(vector<int>& Snake, int fruit1, int fruit2)
{
    unsigned int retVal = 0;
    int snakeSz = Snake.size();
    unsigned int ch = 179, prevCh = 0;
    short prevDiff = 0, nextDiff = 0, diff= 0;
    
    //224 α //249 ∙
    rewriteAt(abs(fruit1), fruit1 >= 0 ? 224 : ' ');
    rewriteAt(abs(fruit2), fruit2 >= 0 ? 249 : ' ');

    for(int i = 1; i <= snakeSz; i++)
    {
        prevDiff = Snake[i-1] - Snake[i-2];
        nextDiff = Snake[i] - Snake[i-1];


        if(i == 1)
        {
            ch = ' ';
        }
        else if(i == 2)
        {
            if(abs(nextDiff) == 1 || abs(nextDiff) == n-1) ch = 196;
            else ch = 179;
        }
        else
        {
            //217 ┘ 218 ┌ 191 ┐ 192 └ 179 │ 196 ─
            diff = prevDiff - nextDiff;
            
            if(diff == -n-1) ch = 218;
            else if(diff == -n+1 ) ch = 191;
            else if(diff == n+1) ch = 217;
            else if(diff == n-1) ch = 192;
            else
            {
                if(abs(prevDiff) == 1 || abs(nextDiff) == 1) ch = 196;
                else if(prevDiff == n && nextDiff == n-1) ch = 217;
                else if(prevDiff == n && nextDiff == -(n-1)) ch = 192;
                else if(prevDiff == -n && nextDiff == n-1) ch = 191;
                else if(prevDiff == -n && nextDiff == -(n-1)) ch = 218;
                else if(prevDiff == n-1 && nextDiff == n) ch = 218;
                else if(prevDiff == n-1 && nextDiff == -n) ch = 192;
                else if(prevDiff == -n+1 && nextDiff == -n) ch = 217;
                else if(prevDiff == -n+1 && nextDiff == n) ch = 191;
                else ch = 179;
            }
        }

        if(i == snakeSz)
        {
            prevCh = matrixCourse[detectRow(Snake[i-1], n)][(Snake[i-1])%n+1];
            ch = 'o';
            if(prevCh == 224)
            {   
                retVal = 1;
            }
            else if(prevCh == 249)
            {
                retVal = 2;
            }
            else if(prevCh != ' ')
            {
                ch = 'X';
                retVal = -1;
            }
    
        }
        rewriteAt(Snake[i-1], ch);
    }

    for(int i = 0; i <= m+1; i++)
    {   
        for(int j = 0; j <= n+1; j++)
        {
           cout << matrixCourse[i][j]; 
        }
        cout << '\n';
    }
    return retVal;
}

