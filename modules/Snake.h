
#include <iostream>
#include <vector>

using namespace std;

class Snake
{
    private:
        vector<int> vSnake;
    public:
        Snake();
        ~Snake();
        void resetSnake();
        char searchVt(int& item);
        void popTail(void);
        void growSnake(int pos);
        int getLength(void);
        int getLast(void);
        vector<int>& getSnake(void);
};