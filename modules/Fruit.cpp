#include "Fruit.h"

void Fruit::resetFruit(int pts)
{
    points = pts;
    pos = 0;
}

void Fruit::setPos(int p)
{
    pos = p;
}

int Fruit::getPos() const
{
    return pos;
}

int Fruit::getPoints() const
{
    return points;
}

void Cherry::setLifetime(short lft)
{
    lifetime = lft;
}

short Cherry::getLifetime() const
{
    return lifetime;
}

void Cherry::minusLifetime()
{
    lifetime -= 1;    

    if(lifetime == 0)
    {
        spawned = 0;
        pos = -pos;
    }
}

char Cherry::isSpawned(void)
{
    return spawned;
}

void Cherry::despawn(void)
{
    spawned = 0;
    pos = -pos;
    lifetime = 0;
}

void Cherry::resetCherry(int pts, short int lft)
{
    points = pts;
    pos = 0;
    spawned = 1;
    lifetime = lft;
    respawnTime = 0;
}

char Cherry::respawn(short int t)
{
    if(!respawnTime) respawnTime = t;

    if(lifetime++ != respawnTime)
    {
        return 0;
    }
    else
    {
        lifetime += 50;
        respawnTime = 0;
        spawned = 1;
        return 1;
    }
}