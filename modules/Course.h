
#include <iostream>
#include <vector>

using namespace std;

class Course
{
    protected:
        int m, n;
        unsigned char** matrixCourse;
    public:
        void drawCourse() const;
        virtual ~Course();

};

class SnakeCourse : public Course
{
    public:
        SnakeCourse(int m, int n);
        void clearMatrix();
        void rewriteAt(int i, unsigned char val);
        char drawCourse(vector<int>& Snake, int fruit1, int fruit2);
};