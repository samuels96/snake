#include "Player.h"
#include <fstream>

Player::Player()
{
    currScore = 0;
    highScore = 0;
}

void Player::saveHighScore()
{
    fstream file;
    file.open("player.data", ios::out);
    file << highScore;
    file.close();
}

void Player::readHighScore()
{
    fstream file;
    file.open("player.data", ios::in);
    file >> highScore;
    file.close();
}

void Player::resetCurrScore()
{
    currScore = 0;
}

void Player::resetHighScore()
{
    highScore = 0;
}

unsigned int Player::getCurrScore()
{
    return currScore;
}

void Player::setCurrScore(unsigned int sc)
{
    currScore = sc;
    
    if(isHighScore())
    {
            setHighScore(currScore);
    }
}

void Player::addCurrScore(unsigned int sc)
{
    currScore += sc;
    
    if(isHighScore())
    {
            setHighScore(currScore);
    }
}


unsigned int Player::getHighScore()
{
    return highScore;
}

void Player::setHighScore(unsigned int sc)
{
    highScore = sc;
}

char Player::isHighScore()
{
    return currScore > highScore;
}