#include <windows.h>

void inline invisCursor()
{
    HANDLE hOut;
    CONSOLE_CURSOR_INFO ConCurInf;
    
    hOut = GetStdHandle(STD_OUTPUT_HANDLE);
    
    ConCurInf.dwSize = 10;
    ConCurInf.bVisible = FALSE;
    
    SetConsoleCursorInfo(hOut, &ConCurInf);
}

void inline gotoxy( int column, int line )
  {
  COORD coord;
  coord.X = column;
  coord.Y = line;
  SetConsoleCursorPosition(
    GetStdHandle( STD_OUTPUT_HANDLE ),
    coord
    );
  }

int inline wherex()
  {
  CONSOLE_SCREEN_BUFFER_INFO csbi;
  COORD  result;
  if (!GetConsoleScreenBufferInfo(
         GetStdHandle( STD_OUTPUT_HANDLE ),
         &csbi
         ))
    return -1;
  return result.X;
  }

int inline wherey()
  {
  CONSOLE_SCREEN_BUFFER_INFO csbi;
  COORD  result;
  if (!GetConsoleScreenBufferInfo(
         GetStdHandle( STD_OUTPUT_HANDLE ),
         &csbi
         ))
    return -1;
  return result.Y;
  }
  
  