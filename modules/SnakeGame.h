#include "Player.h"
#include "Snake.h"
#include "Course.h"
#include "Fruit.h"
#include <conio.h>

using namespace std;

enum Direction{ leftD, upD, rightD, downD };

class SnakeGame
{
    private:
        int m, n;
        Player* gamePlayer;
        Snake* gameSnake;
        SnakeCourse* gameCourse;
        Apple* apple;
        Cherry* cherry;
        Direction lastDirection;

    public:
        SnakeGame(int m, int n);
        void startGame();
        void deleteObjs();
        void resetGame();
        int generatePos();
        Direction nextDirection();
        char nextTurn();
};