#include "modules/SnakeGame.h"
#include <windows.h>

#define WINDOW_WIDTH 40
#define WINDOW_HEIGHT 25

void initWindow()
{
    HANDLE wHnd;
    HANDLE rHnd; 
    SMALL_RECT windowSize = {0, 0, WINDOW_WIDTH, WINDOW_HEIGHT};
    COORD bufferSize = {WINDOW_WIDTH, WINDOW_HEIGHT};
    wHnd = GetStdHandle(STD_OUTPUT_HANDLE);
    rHnd = GetStdHandle(STD_INPUT_HANDLE);
    SetConsoleTitle("Snake Game");
    SetConsoleWindowInfo(wHnd, TRUE, &windowSize);
    SetConsoleScreenBufferSize(wHnd, bufferSize);
    ShowScrollBar(GetConsoleWindow(), SB_BOTH, FALSE);
}

int main()
{
    initWindow();
    SnakeGame sg(WINDOW_HEIGHT-10, WINDOW_WIDTH-10);
    sg.startGame();
    return 0;
}
